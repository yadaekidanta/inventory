<?php

namespace Database\Seeders;

use App\Models\ProductUnit;
use Illuminate\Database\Seeder;

class ProductUnitSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'name' => 'BTL',
            ],
            [
                'name' => 'DUS',
            ],
            [
                'name' => 'JRG',
            ],
            [
                'name' => 'KG',
            ],
            [
                'name' => 'LS',
            ],
            [
                'name' => 'PACK',
            ],
            [
                'name' => 'PAIL',
            ],
            [
                'name' => 'POUCH',
            ],
            [
                'name' => 'TIN',
            ],
            [
                'name' => 'ZAK',
            ],
        );
        foreach($data AS $d){
            ProductUnit::create([
                'name' => $d['name'],
            ]);
        }
    }
}
