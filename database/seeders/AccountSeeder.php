<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;

class AccountSeeder extends Seeder
{
    public function run()
    {
        Account::create([
            'account_type_id' => 7,
            'code' => '101',
            'name' => 'Kas',
            'balance' => 50000000,
        ]);
    }
}
