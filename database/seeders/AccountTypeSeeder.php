<?php

namespace Database\Seeders;

use App\Models\AccountType;
use Illuminate\Database\Seeder;

class AccountTypeSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'name' => 'Aktiva',
                'account_type' => 0,
            ],
            [
                'name' => 'Kewajiban & Pemegang Saham Ekuitas',
                'account_type' => 0,
            ],
            [
                'name' => 'Pendapatan & Biaya Pendapatan',
                'account_type' => 0,
            ],
            [
                'name' => 'Biaya Operasi',
                'account_type' => 0,
            ],
            [
                'name' => 'Pendapatan & Beban Lainnya',
                'account_type' => 0,
            ],
            [
                'name' => 'Akun Sementara',
                'account_type' => 0,
            ],
            [
                'name' => 'Kas',
                'account_type' => 1,
            ],
        );
        foreach($data AS $d){
            AccountType::create([
                'name' => $d['name'],
                'account_type_id' => $d['account_type'],
            ]);
        }
    }
}
