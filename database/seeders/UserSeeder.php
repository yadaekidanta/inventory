<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            'email' => 'yadaekidanta@gmail.com',
            'username' => 'super_admin',
            'password' => Hash::make('password'),
            'role_id' => '1',
        ]);
    }
}
