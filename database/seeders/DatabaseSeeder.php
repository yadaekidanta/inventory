<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            RegionalSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
            AccountTypeSeeder::class,
            AccountSeeder::class,
            ProductUnitSeeder::class,
        ]);
    }
}
