<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        $data = array(
            [
                'name' => 'Super Admin',
            ],
            [
                'name' => 'Administrator',
            ],
            [
                'name' => 'Pegawai',
            ],
        );
        foreach($data AS $d){
            Role::create([
                'name' => $d['name'],
            ]);
        }
    }
}
