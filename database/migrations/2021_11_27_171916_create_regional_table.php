<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionalTable extends Migration
{
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->id('id');
            $table->string('name');
        });
        Schema::create('cities', function (Blueprint $table) {
            $table->id('id');
            $table->integer('province_id');
            $table->string('name');
            $table->string('code',6);
        });
        Schema::create('subdistricts', function (Blueprint $table) {
            $table->id('id');
            $table->integer('city_id');
            $table->string('name');
        });
    }
    public function down()
    {
        Schema::dropIfExists('provinces');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('subdistricts');
    }
}
