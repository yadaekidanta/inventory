<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseTable extends Migration
{
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->id();
            $table->string('code',30);
            $table->integer('supplier_id')->default(0);
            $table->date('date');
            $table->string('grand_total',30)->default(0);
            $table->string('total_payment',30)->default(0);
            $table->enum('st',['Tertunda','Dipesan','Diterima'])->default('Tertunda');
            $table->enum('payment_st',['Belum lunas','Lunas'])->default('Belum lunas');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
