<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpensePaymentTable extends Migration
{
    public function up()
    {
        Schema::create('expense_payments', function (Blueprint $table) {
            $table->id();
            $table->integer('expense_id')->default(0);
            $table->integer('account_id')->default(0);
            $table->date('date')->nullable();
            $table->string('total',20)->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('expense_payments');
    }
}
