<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpenseTable extends Migration
{
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->id();
            $table->string('code',30)->nullable();
            $table->integer('expense_category_id')->default(0);
            $table->timestamp('date')->nullable();
            $table->string('grand_total',20)->default(0);
            $table->string('total_payment',30)->default(0);
            $table->string('document')->nullable();
            $table->longText('note')->nullable();
            $table->enum('payment_st',['Belum lunas','Lunas'])->default('Belum lunas');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('expenses');
    }
}
