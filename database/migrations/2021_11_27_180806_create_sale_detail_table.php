<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleDetailTable extends Migration
{
    public function up()
    {
        Schema::create('sale_details', function (Blueprint $table) {
            $table->id();
            $table->integer('sale_id')->default(0);
            $table->integer('product_id')->default(0);
            $table->string('price',20)->default(0);
            $table->string('qty',20)->default(0);
            $table->string('subtotal',20)->default(0);
        });
    }
    public function down()
    {
        Schema::dropIfExists('sale_details');
    }
}
