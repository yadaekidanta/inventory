<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_category_id')->default(0);
            $table->string('product_unit_id')->default(0);
            $table->string('sku',30);
            $table->string('name');
            $table->string('kemasan',20)->nullable()->default(0);
            $table->string('price',20)->nullable()->default(0);
            $table->string('last_sell_price',20)->nullable()->default(0);
            $table->string('stock',20)->nullable()->default(0);
            $table->string('stock_eceran',20)->nullable()->default(0);
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
