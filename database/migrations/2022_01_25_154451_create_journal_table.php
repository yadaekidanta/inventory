<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalTable extends Migration
{
    public function up()
    {
        Schema::create('journals', function (Blueprint $table) {
            $table->id();
            $table->enum('type',['P','S','E']);
            $table->integer('transaction_id')->default(0);
            $table->date('date')->nullable();
            $table->integer('account_id')->default(0);
            $table->string('credit')->default(0);
            $table->string('debit')->default(0);
        });
    }
    public function down()
    {
        Schema::dropIfExists('journals');
    }
}
