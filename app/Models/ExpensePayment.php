<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExpensePayment extends Model
{
    use HasFactory;
    protected $casts = [
        'date' => 'date',
    ];
    public function expense()
    {
        return $this->belongsTo(Expense::class,'expense_id','id');
    }
    public function account()
    {
        return $this->belongsTo(Account::class,'account_id','id');
    }
}
