<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    use HasFactory;
    protected $casts = [
        'date' => 'date',
    ];
    public function client()
    {
        return $this->belongsTo(Client::class,'client_id','id');
    }
    public function detail()
    {
        return $this->hasMany(SaleDetail::class,'sale_id','id');
    }
    public function payments()
    {
        return $this->hasMany(SalePayment::class,'sale_id','id');
    }
}
