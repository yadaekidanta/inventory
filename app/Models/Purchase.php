<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    use HasFactory;
    protected $casts = [
        'date' => 'date',
    ];
    public function supplier()
    {
        return $this->belongsTo(Supplier::class,'supplier_id','id');
    }
    public function detail()
    {
        return $this->hasMany(PurchaseDetail::class,'purchase_id','id');
    }
    public function payments()
    {
        return $this->hasMany(PurchasePayment::class,'purchase_id','id');
    }
}
