<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    public function varian()
    {
        return $this->hasMany(ProductVariation::class,'product_id','id');
    }
    public function kategori()
    {
        return $this->belongsTo(ProductCategory::class,'product_category_id','id');
    }
    public function satuan()
    {
        return $this->belongsTo(ProductUnit::class,'product_unit_id','id');
    }
}
