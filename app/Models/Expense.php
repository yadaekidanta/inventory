<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;
    protected $casts = [
        'date' => 'date',
    ];
    public function category()
    {
        return $this->belongsTo(Account::class,'expense_category_id','id');
    }
    public function payments()
    {
        return $this->hasMany(ExpensePayment::class,'expense_id','id');
    }
}
