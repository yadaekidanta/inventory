<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    use HasFactory;
    public $timestamps = false;
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
    public function varian()
    {
        return $this->belongsTo(ProductVariation::class,'product_variation_id','id');
    }
}