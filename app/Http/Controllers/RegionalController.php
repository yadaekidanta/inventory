<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Subdistrict;
use Illuminate\Http\Request;

class RegionalController extends Controller
{
    public function get_city(Request $request)
    {
        $collection = City::where('province_id',$request->propinsi)->get();
        $list = "<option value=''>Pilih Kota</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
    public function get_subdistrict(Request $request)
    {
        $collection = Subdistrict::where('city_id',$request->kota)->get();
        $list = "<option value=''>Pilih Kecamatan</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id'>$row->name</option>";
        }
        return $list;
    }
}
