<?php

namespace App\Http\Controllers\CRM;

use App\Models\Client;
use App\Helpers\Helper;
use App\Models\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Client::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.client.list', compact('collection'));
        }
        return view('page.client.main');
    }
    public function create()
    {
        $propinsi = Province::get();
        return view('page.client.input', ['data' => new Client, 'propinsi' => $propinsi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'propinsi' => 'required',
            // 'kota' => 'required',
            // 'kecamatan' => 'required',
            // 'alamat' => 'required',
            // 'telp' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            // elseif ($errors->has('propinsi')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('propinsi'),
            //     ]);
            // }
            // elseif ($errors->has('kota')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kota'),
            //     ]);
            // }
            // elseif ($errors->has('kecamatan')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kecamatan'),
            //     ]);
            // }
            // elseif ($errors->has('alamat')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('alamat'),
            //     ]);
            // }
            // elseif ($errors->has('telp')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('telp'),
            //     ]);
            // }
        }
        if ($request->kode){
            $code = $request->kode.'-';
        }else{
            $code = 'CUST-';
        }
        $generator = Helper::code_generator(new Client,$code);
        $data = new Client;
        $data->code = $generator;
        $data->name = $request->name;
        $data->province_id = $request->propinsi;
        $data->city_id = $request->kota;
        $data->subdistrict_id = $request->kecamatan;
        $data->address = $request->alamat;
        $data->telp_1 = $request->telp_1;
        $data->telp_2 = $request->telp_2;
        $data->phone = $request->phone;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan tersimpan',
        ]);
    }
    public function show(Client $client)
    {
        //
    }
    public function edit(Client $client)
    {
        $propinsi = Province::get();
        return view('page.client.input', ['data' => $client, 'propinsi' => $propinsi]);
    }
    public function update(Request $request, Client $client)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'propinsi' => 'required',
            // 'kota' => 'required',
            // 'kecamatan' => 'required',
            // 'alamat' => 'required',
            // 'telp' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            // elseif ($errors->has('propinsi')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('propinsi'),
            //     ]);
            // }
            // elseif ($errors->has('kota')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kota'),
            //     ]);
            // }
            // elseif ($errors->has('kecamatan')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kecamatan'),
            //     ]);
            // }
            // elseif ($errors->has('alamat')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('alamat'),
            //     ]);
            // }
            // elseif ($errors->has('telp')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('telp'),
            //     ]);
            // }
        }
        $client->name = $request->name;
        $client->province_id = $request->propinsi;
        $client->city_id = $request->kota;
        $client->subdistrict_id = $request->kecamatan;
        $client->address = $request->alamat;
        $client->telp_1 = $request->telp_1;
        $client->telp_2 = $request->telp_2;
        $client->phone = $request->phone;
        $client->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan terubah',
        ]);
    }
    public function destroy(Client $client)
    {
        $client->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pelanggan terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $client = Client::where('code','=',$request->code)->first();
        if(!$client){
            return response()->json([
                'alert' => 'info',
                'message' => 'Client tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $client,
                'message' => 'Client ditemukan',
            ]);
        }
    }
    public function get_list()
    {
        $collection = Client::get();
        return view('page.client.list_modal', ['collection' => $collection]);
    }
}
