<?php

namespace App\Http\Controllers\CRM;

use App\Helpers\Helper;
use App\Models\Province;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Supplier::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.supplier.list', compact('collection'));
        }
        return view('page.supplier.main');
    }
    public function create()
    {
        $propinsi = Province::get();
        return view('page.supplier.input', ['data' => new Supplier, 'propinsi' => $propinsi]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'propinsi' => 'required',
            // 'kota' => 'required',
            // 'kecamatan' => 'required',
            // 'alamat' => 'required',
            // 'telp' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            // elseif ($errors->has('propinsi')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('propinsi'),
            //     ]);
            // }
            // elseif ($errors->has('kota')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kota'),
            //     ]);
            // }
            // elseif ($errors->has('kecamatan')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kecamatan'),
            //     ]);
            // }
            // elseif ($errors->has('alamat')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('alamat'),
            //     ]);
            // }
            // elseif ($errors->has('telp')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('telp'),
            //     ]);
            // }
        }
        if ($request->kode){
            $code = $request->kode.'-';
        }else{
            $code = 'SUPP-';
        }
        $generator = Helper::code_generator(new Supplier,$code);
        $data = new Supplier;
        $data->code = $generator;
        $data->name = $request->name;
        $data->province_id = $request->propinsi;
        $data->city_id = $request->kota;
        $data->subdistrict_id = $request->kecamatan;
        $data->address = $request->alamat;
        $data->telp_1 = $request->telp_1;
        $data->telp_2 = $request->telp_2;
        $data->phone = $request->phone;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier tersimpan',
        ]);
    }
    public function show(Supplier $supplier)
    {
        //
    }
    public function edit(Supplier $supplier)
    {
        $propinsi = Province::get();
        return view('page.supplier.input', ['data' => $supplier, 'propinsi' => $propinsi]);
    }
    public function update(Request $request, Supplier $supplier)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            // 'propinsi' => 'required',
            // 'kota' => 'required',
            // 'kecamatan' => 'required',
            // 'alamat' => 'required',
            // 'telp' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            // elseif ($errors->has('propinsi')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('propinsi'),
            //     ]);
            // }
            // elseif ($errors->has('kota')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kota'),
            //     ]);
            // }
            // elseif ($errors->has('kecamatan')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('kecamatan'),
            //     ]);
            // }
            // elseif ($errors->has('alamat')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('alamat'),
            //     ]);
            // }
            // elseif ($errors->has('telp')) {
            //     return response()->json([
            //         'alert' => 'error',
            //         'message' => $errors->first('telp'),
            //     ]);
            // }
        }
        $supplier->name = $request->name;
        $supplier->province_id = $request->propinsi;
        $supplier->city_id = $request->kota;
        $supplier->subdistrict_id = $request->kecamatan;
        $supplier->address = $request->alamat;
        $supplier->telp_1 = $request->telp_1;
        $supplier->telp_2 = $request->telp_2;
        $supplier->phone = $request->phone;
        $supplier->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier terubah',
        ]);
    }
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Supplier terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $supplier = Supplier::where('code','=',$request->code)->first();
        if(!$supplier){
            return response()->json([
                'alert' => 'info',
                'message' => 'Supplier tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $supplier,
                'message' => 'Supplier ditemukan',
            ]);
        }
    }
    public function get_list()
    {
        $collection = Supplier::get();
        return view('page.supplier.list_modal', ['collection' => $collection]);
    }
}
