<?php

namespace App\Http\Controllers\Master;

use App\Helpers\Helper;
use App\Models\Product;
use App\Models\ProductUnit;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Product::where('sku','LIKE','%'.$keywords.'%')->orWhere('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.product.list', compact('collection'));
        }
        return view('page.product.main');
    }
    public function create()
    {
        $satuan = ProductUnit::get();
        return view('page.product.input', ['data' => new Product, 'satuan' => $satuan]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:products,name',
            'satuan' => 'required',
            'kemasan' => 'required',
            'harga' => 'required',
            'harga_jual' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('satuan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('satuan'),
                ]);
            }
            elseif ($errors->has('kemasan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kemasan'),
                ]);
            }
            elseif ($errors->has('harga')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('harga'),
                ]);
            }
            elseif ($errors->has('harga_jual')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('harga_jual'),
                ]);
            }
        }
        if ($request->sku){
            $sku = $request->sku.'-';
        }else{
            $sku = 'BRG-';
        }
        $generator = Helper::code_generator(new Product,$sku);
        $data = new Product;
        $data->sku = $generator;
        $data->name = $request->name;
        $data->product_unit_id = $request->satuan;
        $data->price = Str::remove(',',$request->harga);
        $data->last_sell_price = Str::remove(',',$request->harga_jual);
        $data->stock = Str::remove(',',$request->stok) * Str::remove(',',$request->kemasan) ?? 0;
        $data->kemasan = Str::remove(',',$request->kemasan) ?? 0;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk tersimpan',
        ]);
    }
    public function show(Product $product)
    {
        //
    }
    public function edit(Product $product)
    {
        $satuan = ProductUnit::get();
        return view('page.product.input', ['data' => $product, 'satuan' => $satuan]);
    }
    public function timbang(Product $product)
    {
        return view('page.product.timbang', ['data' => $product]);
    }
    public function timbang_store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jumlah' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jumlah')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jumlah'),
                ]);
            }
        }
        $product = Product::where('id',$request->id)->first();
        $product->stock_eceran = $product->stock_eceran + $request->jumlah * $product->kemasan;
        $product->stock = $product->stock - $request->jumlah * $product->kemasan;
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk berhasil ditimbang',
        ]);
    }
    public function update(Request $request, Product $product)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'satuan' => 'required',
            'kemasan' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('satuan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('satuan'),
                ]);
            }
        }
        $product->name = $request->name;
        $product->product_unit_id = $request->satuan;
        $product->price = Str::remove(',',$request->harga);
        $product->last_sell_price = Str::remove(',',$request->harga_jual);
        $product->stock = Str::remove(',',$request->stok) * Str::remove(',',$request->kemasan) ?? 0;
        $product->kemasan = Str::remove(',',$request->kemasan) ?? 0;
        $product->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terubah',
        ]);
    }
    public function destroy(Product $product)
    {
        $product->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terhapus',
        ]);
    }
    public function get(Request $request)
    {
        $product = Product::where('sku','=',$request->code)->first();
        if(!$product){
            return response()->json([
                'alert' => 'info',
                'message' => 'Product tidak ditemukan',
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'data' => $product,
                'message' => 'Product ditemukan',
            ]);
        }
    }
    public function get_list()
    {
        $collection = Product::get();
        return view('page.product.list_modal', ['collection' => $collection]);
    }
}
