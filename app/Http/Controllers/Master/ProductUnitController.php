<?php

namespace App\Http\Controllers\Master;

use App\Models\ProductUnit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductUnitController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = ProductUnit::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.productUnit.list', compact('collection'));
        }
        return view('page.productUnit.main');
    }
    public function create()
    {
        return view('page.productUnit.input', ['data' => new ProductUnit]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_categories,name',
            'shortname' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('shortname')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shortname'),
                ]);
            }
        }
        $data = new ProductUnit;
        $data->name = $request->name;
        $data->shortname = $request->shortname;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan tersimpan',
        ]);
    }
    public function show(ProductUnit $productUnit)
    {
        //
    }
    public function edit(ProductUnit $productUnit)
    {
        return view('page.productUnit.input', ['data' => $productUnit]);
    }
    public function update(Request $request, ProductUnit $productUnit)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_categories,name',
            'shortname' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('shortname')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('shortname'),
                ]);
            }
        }
        $productUnit->name = $request->name;
        $productUnit->shortname = $request->shortname;
        $productUnit->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan terubah',
        ]);
    }
    public function destroy(ProductUnit $productUnit)
    {
        $productUnit->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Satuan terhapus',
        ]);
    }
}
