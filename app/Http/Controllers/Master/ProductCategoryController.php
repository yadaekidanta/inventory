<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Models\ProductCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = ProductCategory::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.productCategory.list', compact('collection'));
        }
        return view('page.productCategory.main');
    }
    public function create()
    {
        return view('page.productCategory.input', ['data' => new ProductCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_categories,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $data = new ProductCategory;
        $data->name = $request->name;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori tersimpan',
        ]);
    }
    public function show(ProductCategory $productCategory)
    {
        //
    }
    public function edit(ProductCategory $productCategory)
    {
        return view('page.productCategory.input', ['data' => $productCategory]);
    }
    public function update(Request $request, ProductCategory $productCategory)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:product_categories,name',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
        }
        $productCategory->name = $request->name;
        $productCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terubah',
        ]);
    }
    public function destroy(ProductCategory $productCategory)
    {
        $productCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Kategori terhapus',
        ]);
    }
}
