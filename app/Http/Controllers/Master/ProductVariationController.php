<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\ProductVariation;
use Illuminate\Http\Request;

class ProductVariationController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(ProductVariation $productVariation)
    {
        //
    }
    public function edit(ProductVariation $productVariation)
    {
        //
    }
    public function update(Request $request, ProductVariation $productVariation)
    {
        //
    }
    public function destroy(ProductVariation $productVariation)
    {
        $id = $productVariation->product_id;
        $productVariation->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Produk terhapus',
            'redirect'=> 'input',
            'route'=> route('app.product.edit',$id),
        ]);
    }
    public function get_list(Request $request)
    {
        $collection = ProductVariation::where('product_id',$request->product)->get();
        $list = "<option>Pilih Varian</option>";
        foreach($collection as $row){
            $list.="<option value='$row->id' data-price='$row->price' data-sellprice='$row->last_sell_price'>".$row->name. ' ' .$row->product->satuan->shortname."</option>";
        }
        return $list;
    }
}
