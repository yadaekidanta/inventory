<?php

namespace App\Http\Controllers\Finance;

use App\Models\Account;
use App\Models\AccountType;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Account::where('code','LIKE','%'.$keywords.'%')->orWhere('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.account.list', compact('collection'));
        }
        return view('page.account.main');
    }
    public function create()
    {
        $type = AccountType::get();
        return view('page.account.input', ['data' => new Account, 'type' => $type]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'saldo' => 'required',
            'tipe' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode'),
                ]);
            }
            elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
            elseif ($errors->has('saldo')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('saldo'),
                ]);
            }
            elseif ($errors->has('tipe')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tipe'),
                ]);
            }
        }
        $data = new Account;
        $data->code = $request->kode;
        $data->name = $request->nama;
        $data->balance = Str::remove(',',$request->saldo) ?? 0;
        $data->account_type_id = $request->tipe ?? 0;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun tersimpan',
        ]);
    }
    public function show(Account $account)
    {
        //
    }
    public function edit(Account $account)
    {
        $type = AccountType::get();
        return view('page.account.input', ['data' => $account, 'type' => $type]);
    }
    public function update(Request $request, Account $account)
    {
        $validator = Validator::make($request->all(), [
            'kode' => 'required',
            'nama' => 'required',
            'tipe' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('kode')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kode'),
                ]);
            }
            elseif ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
            elseif ($errors->has('tipe')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tipe'),
                ]);
            }
        }
        $account->code = $request->kode;
        $account->name = $request->nama;
        $account->account_type_id = $request->tipe ?? 0;
        $account->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun terubah',
        ]);
    }
    public function destroy(Account $account)
    {
        $account->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Akun terhapus',
        ]);
    }
}
