<?php

namespace App\Http\Controllers\Finance;

use App\Models\Sale;
use App\Models\Product;
use App\Models\SalePayment;
use Illuminate\Http\Request;
use App\Models\ExpensePayment;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;

class IncomeStatementController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $pp = PurchasePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $s = Sale::where('date','LIKE','%'.$keywords.'%')->get();
            $sp = SalePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $ep = ExpensePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $pa = 0;
            foreach($s as $sale){
                foreach($sale->detail as $detail){
                    if($detail->tipe == "Eceran"){
                        $pa += $detail->product->price * $detail->qty;
                    }else{
                        $pa += $detail->product->price * ($detail->qty * $detail->product->kemasan);
                    }
                }
            }
            $total_persediaan = 0;
            $tp = Product::get();
            foreach($tp as $t){
                $total_persediaan += (floatval($t->stock)+floatval($t->stock_eceran)) * floatval($t->price);
            }
            return view('page.incomeStatement.list', compact('pp','sp','ep','pa','total_persediaan'));
        }
        return view('page.incomeStatement.main');
    }
}