<?php

namespace App\Http\Controllers\Finance;

use App\Models\Sale;
use App\Models\Product;
use App\Models\SalePayment;
use Illuminate\Http\Request;
use App\Models\ExpensePayment;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;
use App\Models\Account;

class BalanceSheetController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $kas = Account::where('id',1)->first();
            $piutang = Account::where('id',2)->first();
            $peralatan = Account::where('id',11)->first();
            $bangunan = Account::where('id',4)->first();
            $hutang_dagang = Account::where('id',5)->first();
            $hutang_gaji = Account::where('id',6)->first();
            $modal = Account::where('id',7)->first();
            $laba = Account::where('id',8)->first();
            $pp = PurchasePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $s = Sale::where('date','LIKE','%'.$keywords.'%')->get();
            $sp = SalePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $ep = ExpensePayment::where('date','LIKE','%'.$keywords.'%')->get();
            $pa = 0;
            foreach($s as $sale){
                foreach($sale->detail as $detail){
                    if($detail->tipe == "Eceran"){
                        $pa += $detail->product->price * $detail->qty;
                    }else{
                        $pa += $detail->product->price * ($detail->qty * $detail->product->kemasan);
                    }
                }
            }
            $total_persediaan = 0;
            $tp = Product::get();
            foreach($tp as $t){
                $total_persediaan += (floatval($t->stock)+floatval($t->stock_eceran)) * floatval($t->price);
            }
            return view('page.balanceSheet.list', compact('kas','pp','sp','ep','pa','total_persediaan','piutang','peralatan','bangunan','hutang_dagang','hutang_gaji','modal','laba'));
        }
        return view('page.balanceSheet.main');
    }
}
