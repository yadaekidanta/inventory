<?php

namespace App\Http\Controllers\Finance;

use App\Models\AccountType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AccountTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = AccountType::where('name','LIKE','%'.$keywords.'%')->where('account_type_id',0)->orderBy('id', 'ASC')->paginate(10);
            return view('page.accountType.list', compact('collection'));
        }
        return view('page.accountType.main');
    }
    public function create()
    {
        $type = AccountType::where('account_type_id',0)->get();
        return view('page.accountType.input', ['data' => new AccountType, 'type' => $type]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $data = new AccountType;
        $data->name = $request->nama;
        $data->account_type_id = $request->tipe ?? 0;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Akun tersimpan',
        ]);
    }
    public function show(AccountType $accountType)
    {
        //
    }
    public function edit(AccountType $accountType)
    {
        $type = AccountType::where('account_type_id', 0)->get();
        return view('page.accountType.input', ['data' => $accountType, 'type' => $type]);
    }
    public function update(Request $request, AccountType $accountType)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $accountType->name = $request->nama;
        $accountType->account_type_id = $request->tipe ?? 0;
        $accountType->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Akun terubah',
        ]);
    }
    public function destroy(AccountType $accountType)
    {
        $accountType->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Akun terhapus',
        ]);
    }
}
