<?php

namespace App\Http\Controllers\Finance;

use App\Models\SalePayment;
use Illuminate\Http\Request;
use App\Models\ExpensePayment;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;

class CashFlowController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $pp = PurchasePayment::where('account_id',1)->where('date','LIKE','%'.$keywords.'%')->get();
            $sp = SalePayment::where('account_id',1)->where('date','LIKE','%'.$keywords.'%')->get();
            $ep = ExpensePayment::where('account_id',1)->where('date','LIKE','%'.$keywords.'%')->get();
            return view('page.cashFlow.list', compact('pp','sp','ep'));
        }
        return view('page.cashFlow.main');
    }
}
