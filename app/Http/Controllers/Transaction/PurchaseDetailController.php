<?php

namespace App\Http\Controllers\Transaction;

use App\Models\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PurchaseDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PurchaseDetailController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang' => 'required',
            'harga' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('barang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('barang'),
                ]);
            }
            elseif ($errors->has('harga')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('harga'),
                ]);
            }
            elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $qty = 0;
        $product = Product::where('id',$request->barang)->first();
        $cek = PurchaseDetail::where('purchase_id',$request->id)->where('product_id',$request->barang)->first();
        if($cek){
            $data = $cek;
            $qty = $cek->qty + $request->qty;
        }else{
            $data = new PurchaseDetail;
            $qty = $request->qty;
        }
        $data->qty = $qty;
        $data->purchase_id = $request->id;
        $data->product_id = $request->barang;
        $data->price = Str::remove(',',$request->harga) * $product->kemasan;
        $data->subtotal = Str::remove(',',$request->harga) * $qty * $product->kemasan;
        if($cek){
            $data->update();
        }else{
            $data->save();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang tersimpan',
            'redirect' => 'input',
            'route' => route('app.purchase.show',$request->id),
        ]);
    }
    public function show(PurchaseDetail $purchaseDetail)
    {
        //
    }
    public function edit(PurchaseDetail $purchaseDetail)
    {
        //
    }
    public function update(Request $request, PurchaseDetail $purchaseDetail)
    {
        //
    }
    public function destroy(PurchaseDetail $purchaseDetail)
    {
        $id = $purchaseDetail->purchase_id;
        $purchaseDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('app.purchase.show',$id),
        ]);
    }
}
