<?php

namespace App\Http\Controllers\Transaction;

use App\Models\Sale;
use App\Models\Client;
use App\Helpers\Helper;
use App\Models\Account;
use App\Models\SaleDetail;
use App\Models\SalePayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SaleController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Sale::where('code','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.sale.list', compact('collection'));
        }
        return view('page.sale.main');
    }
    public function create()
    {
        $client = Client::get();
        return view('page.sale.input', ['data' => new Sale, 'client' => $client]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pelanggan' => 'required',
            'tanggal' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('pelanggan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pelanggan'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
        }
        if ($request->kode){
            $code = $request->kode.'-';
        }else{
            $code = 'SALE-';
        }
        $generator = Helper::code_generator(new Sale,$code);
        $data = new Sale;
        $data->code = $generator;
        $data->client_id = $request->pelanggan;
        $data->date = $request->tanggal;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan tersimpan',
            'redirect' => 'input',
            'route' => route('app.sale.edit',$data->id),
        ]);
    }
    public function show(Sale $sale)
    {
        $akun = Account::get();
        return view('page.sale.show', ['data' => $sale, 'akun' => $akun]);
    }
    public function edit(Sale $sale)
    {
        $client = Client::get();
        return view('page.sale.input', ['data' => $sale, 'client' => $client]);
    }
    public function update(Request $request, Sale $sale)
    {
        $validator = Validator::make($request->all(), [
            'pelanggan' => 'required',
            'tanggal' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('pelanggan')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('pelanggan'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
        }
        $sale->client_id = $request->pelanggan;
        $sale->date = $request->tanggal;
        $sale->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan terubah',
        ]);
    }
    public function destroy(Sale $sale)
    {
        Helper::increase_stock_sale(new SaleDetail,'sale_id', $sale->id);
        SaleDetail::where('sale_id', $sale->id)->delete();
        $sale->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan terhapus',
        ]);
    }
    public function process(Sale $sale)
    {
        $cek = SaleDetail::where('sale_id',$sale->id)->get()->count();
        if($cek > 0){
            $gt = SaleDetail::where('sale_id',$sale->id)->get()->sum('subtotal');
            $sale->grand_total = $gt;
            $sale->st = 'Dipesan';
            $sale->update();
            return response()->json([
                'alert' => 'success',
                'message' => 'Penjualan diproses',
                'redirect' => 'input',
                'route' => route('app.sale.show',$sale->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Belum ada barang',
                'redirect' => 'input',
                'route' => route('app.sale.show',$sale->id),
            ]);
        }
    }
    public function finish(Sale $sale)
    {
        Helper::decrease_stock_sale(new SaleDetail,'sale_id', $sale->id);
        $sale->st = 'Diterima';
        $sale->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Penjualan selesai',
            'redirect' => 'input',
            'route' => route('app.sale.show',$sale->id),
        ]);
    }
    public function process_payment(Sale $sale)
    {
        $cek = SalePayment::where('sale_id',$sale->id)->get();
        if($cek->count() > 0){
            if($cek->sum('total') < $sale->grand_total){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Pembayaran belum lunas',
                ]);
            }else{
                $sale->st_payment = 'Lunas';
                $sale->update();
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Pembayaran lunas',
                ]);
            }
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Belum ada pembayaran',
            ]);
        }
    }
}
