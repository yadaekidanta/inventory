<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use App\Models\Purchase;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PurchasePaymentController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'akun' => 'required',
            'tanggal_bayar' => 'required',
            'jumlah_bayar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('akun')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akun'),
                ]);
            }
            elseif ($errors->has('tanggal_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_bayar'),
                ]);
            }
            elseif ($errors->has('jumlah_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jumlah_bayar'),
                ]);
            }
        }
        $total = 0;
        $grand_total = Purchase::where('id',$request->id)->get()->sum('grand_total');
        $total_payment = Purchase::where('id',$request->id)->get()->sum('total_payment');
        $cek = PurchasePayment::where('purchase_id',$request->id)->where('account_id',$request->akun)->where('date', $request->tanggal_bayar)->first();
        if($cek){
            $data = $cek;
            $total = $cek->total + Str::remove(',',$request->jumlah_bayar);
        }else{
            $data = new PurchasePayment;
            $total = Str::remove(',',$request->jumlah_bayar);
        }
        $cek_saldo = Helper::check_balance($request->akun,$total);
        if($cek_saldo == 1){
            if($total_payment + Str::remove(',',$request->jumlah_bayar) > $grand_total){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Jumlah pembayaran lebih ' . number_format($total_payment + $total - $grand_total),
                ]);
            }            
            $data->date = $request->tanggal_bayar;
            $data->total = $total;
            $data->note = $request->catatan;
            $data->purchase_id = $request->id;
            $data->account_id = $request->akun;
            if($cek){
                $data->update();
            }else{
                $data->save();
            }
            Helper::decrease_balance(new PurchasePayment, $request->akun, Str::remove(',',$request->jumlah_bayar), new Purchase,'purchase_id',$request->id,"Pembelian");
            return response()->json([
                'alert' => 'success',
                'message' => 'Pembayaran tersimpan',
                'redirect' => 'input',
                'route' => route('app.purchase.show',$request->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'success',
                'message' => 'Saldo Tidak Cukup',
                'redirect' => 'input',
                'route' => route('app.purchase.show',$request->id),
            ]);
        }
    }
    public function show(PurchasePayment $purchasePayment)
    {
        //
    }
    public function edit(PurchasePayment $purchasePayment)
    {
        //
    }
    public function update(Request $request, PurchasePayment $purchasePayment)
    {
        //
    }
    public function destroy(PurchasePayment $purchasePayment)
    {
        $id_account = $purchasePayment->account_id;
        $total = $purchasePayment->total;
        $id_purchase = $purchasePayment->purchase_id;
        $purchasePayment->delete();
        Helper::increase_balance(new PurchasePayment, $id_account, $total, new Purchase,'purchase_id',$id_purchase,"Pembelian");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('app.purchase.show',$id_purchase),
        ]);
    }
}
