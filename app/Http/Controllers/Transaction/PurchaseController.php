<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use App\Models\Account;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Models\PurchaseDetail;
use App\Models\PurchasePayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PurchaseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Purchase::where('code','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.purchase.list', compact('collection'));
        }
        return view('page.purchase.main');
    }
    public function create()
    {
        $supplier = Supplier::get();
        return view('page.purchase.input', ['data' => new Purchase, 'supplier' => $supplier]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier' => 'required',
            'tanggal' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('supplier')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('supplier'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
        }
        if ($request->kode){
            $code = $request->kode.'-';
        }else{
            $code = 'PRCS-';
        }
        $generator = Helper::code_generator(new Purchase,$code);
        $data = new Purchase;
        $data->code = $generator;
        $data->supplier_id = $request->supplier;
        $data->date = $request->tanggal;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian tersimpan',
        ]);
    }
    public function show(Purchase $purchase)
    {
        $akun = Account::get();
        return view('page.purchase.show', ['data' => $purchase, 'akun' => $akun]);
    }
    public function edit(Purchase $purchase)
    {
        $supplier = Supplier::get();
        return view('page.purchase.input', ['data' => $purchase, 'supplier' => $supplier]);
    }
    public function update(Request $request, Purchase $purchase)
    {
        $validator = Validator::make($request->all(), [
            'supplier' => 'required',
            'tanggal' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('supplier')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('supplier'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
        }
        $purchase->supplier_id = $request->supplier;
        $purchase->date = $request->tanggal;
        $purchase->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian terubah',
        ]);
    }
    public function destroy(Purchase $purchase)
    {
        Helper::decrease_stock_purchase(new PurchaseDetail,'purchase_id', $purchase->id);
        PurchaseDetail::where('purchase_id', $purchase->id)->delete();
        $purchase->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian terhapus',
        ]);
    }
    public function process(Purchase $purchase)
    {
        $cek = PurchaseDetail::where('purchase_id',$purchase->id)->get()->count();
        if($cek > 0){
            $gt = PurchaseDetail::where('purchase_id',$purchase->id)->get()->sum('subtotal');
            $purchase->grand_total = $gt;
            $purchase->st = 'Dipesan';
            $purchase->update();
            return response()->json([
                'alert' => 'success',
                'message' => 'Pembelian diproses',
                'redirect' => 'input',
                'route' => route('app.purchase.show',$purchase->id),
            ]);
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Belum ada barang',
                'redirect' => 'input',
                'route' => route('app.purchase.show',$purchase->id),
            ]);
        }
    }
    public function finish(purchase $purchase)
    {
        Helper::increase_stock_purchase(new PurchaseDetail,'purchase_id', $purchase->id);
        $purchase->st = 'Diterima';
        $purchase->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembelian selesai',
        ]);
    }
    public function process_payment(Purchase $purchase)
    {
        $cek = PurchasePayment::where('purchase_id',$purchase->id)->get();
        if($cek->count() > 0){
            if($cek->sum('total') < $purchase->grand_total){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Pembayaran belum lunas',
                ]);
            }else{
                $purchase->st_payment = 'Lunas';
                $purchase->update();
                return response()->json([
                    'alert' => 'success',
                    'message' => 'Pembayaran lunas',
                ]);
            }
        }else{
            return response()->json([
                'alert' => 'info',
                'message' => 'Belum ada pembayaran',
            ]);
        }
    }
}
