<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use App\Models\Expense;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ExpensePayment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExpensePaymentController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'akun' => 'required',
            'tanggal_bayar' => 'required',
            'jumlah_bayar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('akun')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akun'),
                ]);
            }
            elseif ($errors->has('tanggal_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_bayar'),
                ]);
            }
            elseif ($errors->has('jumlah_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jumlah_bayar'),
                ]);
            }
        }
        $total = 0;
        $grand_total = Expense::where('id',$request->id)->get()->sum('grand_total');
        $total_payment = Expense::where('id',$request->id)->get()->sum('total_payment');
        $cek = ExpensePayment::where('expense_id',$request->id)->where('account_id',$request->akun)->where('date', $request->tanggal_bayar)->first();
        if($cek){
            $data = $cek;
            $total = $cek->total + Str::remove(',',$request->jumlah_bayar);
        }else{
            $data = new ExpensePayment;
            $total = Str::remove(',',$request->jumlah_bayar);
        }
        $cek_saldo = Helper::check_balance($request->akun,$total);
        if($cek_saldo == 1){
            if($total_payment + Str::remove(',',$request->jumlah_bayar) > $grand_total){
                return response()->json([
                    'alert' => 'info',
                    'message' => 'Jumlah pembayaran lebih ' . number_format($total_payment + $total - $grand_total),
                ]);
            }            
            $data->date = $request->tanggal_bayar;
            $data->total = $total;
            $data->expense_id = $request->id;
            $data->account_id = $request->akun;
            if($cek){
                $data->update();
            }else{
                $data->save();
            }
            Helper::decrease_balance(new ExpensePayment, $request->akun, Str::remove(',',$request->jumlah_bayar), new Expense,'expense_id',$request->id,"Pengeluaran");
            return response()->json([
                'alert' => 'success',
                'message' => 'Pembayaran tersimpan',
                'redirect' => 'input',
                'route' => route('app.expense.edit',$request->id),
            ]);
        }
    }
    public function show(ExpensePayment $expensePayment)
    {
        //
    }
    public function edit(ExpensePayment $expensePayment)
    {
        //
    }
    public function update(Request $request, ExpensePayment $expensePayment)
    {
        //
    }
    public function destroy(ExpensePayment $expensePayment)
    {
        $id_account = $expensePayment->account_id;
        $total = $expensePayment->total;
        $id_expense = $expensePayment->expense_id;
        $expensePayment->delete();
        Helper::increase_balance(new ExpensePayment, $id_account, $total, new Expense,'expense_id',$id_expense,"Pengeluaran");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('app.expense.edit',$id_expense),
        ]);
    }
}
