<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use App\Models\Product;
use App\Models\SaleDetail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SaleDetailController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barang' => 'required',
            'harga' => 'required',
            'qty' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('barang')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('barang'),
                ]);
            }
            elseif ($errors->has('harga')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('harga'),
                ]);
            }
            elseif ($errors->has('qty')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('qty'),
                ]);
            }
        }
        $qty = 0;
        $product = Product::where('id',$request->barang)->first();
        $cek = SaleDetail::where('sale_id',$request->id)->where('product_id',$request->barang)->where('tipe',$request->satuan)->first();
        if($cek){
            $data = $cek;
            $qty = $cek->qty + $request->qty;
        }else{
            $data = new SaleDetail;
            $qty = $request->qty;
        }
        $cek_stok = Helper::check_stock($request->barang,$qty,$request->satuan);
        if($cek_stok == 0){
            return response()->json([
				'alert' => 'info',
				'message' => 'Stok barang tidak cukup',
			]);
        }
        $data->qty = $qty;
        $data->sale_id = $request->id;
        $data->product_id = $request->barang;
        if($request->satuan == 1){
            $data->tipe = "Utuh";
            $data->price = Str::remove(',',$request->harga) * $product->kemasan;
            $data->subtotal = Str::remove(',',$request->harga) * $qty * $product->kemasan;
        }else{
            $data->tipe = "Eceran";
            $data->price = Str::remove(',',$request->harga);
            $data->subtotal = Str::remove(',',$request->harga) * $qty;
        }
        if($cek){
            $data->update();
        }else{
            $data->save();
        }
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang tersimpan',
            'redirect' => 'input',
            'route' => route('app.sale.show',$request->id),
        ]);
    }
    public function show(SaleDetail $saleDetail)
    {
        //
    }
    public function edit(SaleDetail $saleDetail)
    {
        //
    }
    public function update(Request $request, SaleDetail $saleDetail)
    {
        //
    }
    public function destroy(SaleDetail $saleDetail)
    {
        $id = $saleDetail->sale_id;
        $saleDetail->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Barang terhapus',
            'redirect' => 'input',
            'route' => route('app.sale.show',$id),
        ]);
    }
}
