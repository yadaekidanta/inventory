<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Models\ExpenseCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ExpenseCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = ExpenseCategory::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.expenseCategory.list', compact('collection'));
        }
        return view('page.expenseCategory.main');
    }
    public function create()
    {
        return view('page.expenseCategory.input', ['data' => new ExpenseCategory]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.'-';
        }else{
            $code = 'EC-';
        }
        $generator = Helper::code_generator(new ExpenseCategory,$code);
        $data = new ExpenseCategory;
        $data->code = $generator;
        $data->name = $request->nama;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran tersimpan',
        ]);
    }
    public function show(ExpenseCategory $expenseCategory)
    {
        //
    }
    public function edit(ExpenseCategory $expenseCategory)
    {
        return view('page.expenseCategory.input', ['data' => $expenseCategory]);
    }
    public function update(Request $request, ExpenseCategory $expenseCategory)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('nama')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('nama'),
                ]);
            }
        }
        $expenseCategory->name = $request->nama;
        $expenseCategory->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran terubah',
        ]);
    }
    public function destroy(ExpenseCategory $expenseCategory)
    {
        $expenseCategory->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Jenis Pengeluaran terhapus',
        ]);
    }
}
