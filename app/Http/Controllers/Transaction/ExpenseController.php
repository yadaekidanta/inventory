<?php

namespace App\Http\Controllers\Transaction;

use App\Helpers\Helper;
use App\Models\Account;
use App\Models\Expense;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\ExpenseCategory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ExpenseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = Expense::where('code','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.expense.list', compact('collection'));
        }
        return view('page.expense.main');
    }
    public function create()
    {
        $category = Account::where('account_type_id',5)->get();
        return view('page.expense.input', ['data' => new Expense, 'category' => $category]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'jenis_pengeluaran' => 'required',
            'tanggal' => 'required',
            'total_pengeluaran' => 'required',
            'dokumen' => 'mimes:pdf',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jenis_pengeluaran')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_pengeluaran'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
            elseif ($errors->has('total_pengeluaran')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('total_pengeluaran'),
                ]);
            }
            elseif ($errors->has('dokumen')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('dokumen'),
                ]);
            }
        }
        if ($request->code){
            $code = $request->code.'-';
        }else{
            $code = 'EXP-';
        }
        $generator = Helper::code_generator(new Expense,$code);
        $data = new Expense;
        $data->code = $generator;
        $data->date = $request->tanggal;
        $data->expense_category_id = $request->jenis_pengeluaran;
        $data->grand_total = Str::remove(',',$request->total_pengeluaran);
        if(request()->file('dokumen')){
            $dokumen = request()->file('dokumen')->store("dokumen_pengeluaran");
            $data->document = $dokumen;
        }
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran tersimpan',
            'redirect' => 'input',
            'route' => route('app.expense.edit',$data->id),
        ]);
    }
    public function show(Expense $expense)
    {
        //
    }
    public function edit(Expense $expense)
    {
        $akun = Account::get();
        $category = Account::where('account_type_id',5)->get();
        return view('page.expense.input', ['data' => $expense, 'akun' => $akun, 'category' => $category]);
    }
    public function update(Request $request, Expense $expense)
    {
        $validator = Validator::make($request->all(), [
            'jenis_pengeluaran' => 'required',
            'tanggal' => 'required',
            'total_pengeluaran' => 'required',
            'dokumen' => 'mimes:pdf',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('jenis_pengeluaran')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jenis_pengeluaran'),
                ]);
            }
            elseif ($errors->has('tanggal')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal'),
                ]);
            }
            elseif ($errors->has('total_pengeluaran')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('total_pengeluaran'),
                ]);
            }
            elseif ($errors->has('dokumen')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('dokumen'),
                ]);
            }
        }
        $expense->expense_category_id = $request->jenis_pengeluaran;
        $expense->grand_total = Str::remove(',',$request->total_pengeluaran);
        if(request()->file('dokumen')){
            Storage::delete($expense->document);
            $dokumen = request()->file('dokumen')->store("dokumen_pengeluaran");
            $expense->document = $dokumen;
        }
        $expense->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran terubah',
            'redirect' => 'input',
            'route' => route('app.expense.edit',$expense->id),
        ]);
    }
    public function destroy(Expense $expense)
    {
        Storage::delete($expense->document);
        $expense->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengeluaran terhapus',
        ]);
    }
    public function journal(Expense $expense)
    {
        $akun = Account::get();
        $category = ExpenseCategory::get();
        return view('page.expense.journal', ['data' => $expense, 'akun' => $akun, 'category' => $category]);
    }
}
