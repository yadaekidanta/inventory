<?php

namespace App\Http\Controllers\Transaction;

use App\Models\Sale;
use App\Helpers\Helper;
use App\Models\SalePayment;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class SalePaymentController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'akun' => 'required',
            'tanggal_bayar' => 'required',
            'jumlah_bayar' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('akun')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('akun'),
                ]);
            }
            elseif ($errors->has('tanggal_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('tanggal_bayar'),
                ]);
            }
            elseif ($errors->has('jumlah_bayar')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('jumlah_bayar'),
                ]);
            }
        }
        $total = 0;
        $grand_total = Sale::where('id',$request->id)->get()->sum('grand_total');
        $total_payment = Sale::where('id',$request->id)->get()->sum('total_payment');
        $cek = SalePayment::where('sale_id',$request->id)->where('account_id',$request->akun)->where('date', $request->tanggal_bayar)->first();
        if($cek){
            $data = $cek;
            $total = $cek->total + Str::remove(',',$request->jumlah_bayar);
        }else{
            $data = new SalePayment;
            $total = Str::remove(',',$request->jumlah_bayar);
        }
        if($total_payment + Str::remove(',',$request->jumlah_bayar) > $grand_total){
            return response()->json([
                'alert' => 'info',
                'message' => 'Jumlah pembayaran lebih ' . number_format($total_payment + $total - $grand_total),
            ]);
        }            
        $data->date = $request->tanggal_bayar;
        $data->total = $total;
        $data->note = $request->catatan;
        $data->sale_id = $request->id;
        $data->account_id = $request->akun;
        if($cek){
            $data->update();
        }else{
            $data->save();
        }
        Helper::increase_balance(new SalePayment, $request->akun, Str::remove(',',$request->jumlah_bayar), new Sale,'sale_id',$request->id,"Penjualan");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran tersimpan',
            'redirect' => 'input',
            'route' => route('app.sale.show',$request->id),
        ]);
    }
    public function show(SalePayment $salePayment)
    {
        //
    }
    public function edit(SalePayment $salePayment)
    {
        //
    }
    public function update(Request $request, SalePayment $salePayment)
    {
        //
    }
    public function destroy(SalePayment $salePayment)
    {
        $id_account = $salePayment->account_id;
        $total = $salePayment->total;
        $id_sale = $salePayment->sale_id;
        $salePayment->delete();
        Helper::decrease_balance(new SalePayment, $id_account, $total, new Sale,'sale_id',$id_sale,"Penjualan");
        return response()->json([
            'alert' => 'success',
            'message' => 'Pembayaran terhapus',
            'redirect' => 'input',
            'route' => route('app.sale.show',$id_sale),
        ]);
    }
}
