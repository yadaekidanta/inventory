<?php

namespace App\Http\Controllers\HRM;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $keywords = $request->keyword;
            $collection = User::where('name','LIKE','%'.$keywords.'%')->orderBy('id', 'ASC')->paginate(10);
            return view('page.user.list', compact('collection'));
        }
        return view('page.user.main');
    }
    public function create()
    {
        $role = Role::get();
        return view('page.user.input', ['data' => new User, 'role' => $role]);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'id_pengguna' => 'required',
            'email' => 'required',
            'kata_sandi' => 'required',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('id_pengguna')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('id_pengguna'),
                ]);
            }
            elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
            elseif ($errors->has('kata_sandi')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('kata_sandi'),
                ]);
            }
            elseif ($errors->has('role')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }
        }
        $data = new User;
        $data->name = $request->name;
        $data->username = $request->id_pengguna;
        $data->email = $request->email;
        $data->password = Hash::make($request->kata_sandi);
        $data->role_id = $request->role;
        $data->save();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengguna tersimpan',
        ]);
    }
    public function show(User $user)
    {
        //
    }
    public function edit(User $user)
    {
        $role = Role::get();
        return view('page.user.input', ['data' => $user, 'role' => $role]);
    }
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'id_pengguna' => 'required',
            'email' => 'required',
            'role' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('name')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('name'),
                ]);
            }
            elseif ($errors->has('id_pengguna')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('id_pengguna'),
                ]);
            }
            elseif ($errors->has('email')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('email'),
                ]);
            }
            elseif ($errors->has('role')) {
                return response()->json([
                    'alert' => 'error',
                    'message' => $errors->first('role'),
                ]);
            }
        }
        $user->name = $request->name;
        $user->username = $request->id_pengguna;
        $user->email = $request->email;
        if($request->kata_sandi){
            $user->password = Hash::make($request->kata_sandi);
        }
        $user->role_id = $request->role;
        $user->update();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengguna terubah',
        ]);
    }
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'alert' => 'success',
            'message' => 'Pengguna terhapus',
        ]);
    }
}
