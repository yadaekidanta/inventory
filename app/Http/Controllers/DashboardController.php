<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        return view('page.dashboard.main');
    }
    
    public function keuangan(){
        return view('page.keuangan.main');
    }

    public function report(){
        return view('page.report.main');
    }    
}
