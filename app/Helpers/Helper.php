<?php
namespace App\Helpers;

use App\Models\Account;
use App\Models\Journal;
use App\Models\Product;
use App\Models\ProductVariation;

class Helper{
    public static function code_generator($model, $prefix){
        $preff = $prefix;
		$value = '';
		$row = $model::orderBy('id','DESC')->get()->count();
		$nomor = sprintf("%03s", $row) + 1;
		$value = $preff.$nomor;
		return $value;
    }
	public static function check_stock($id_product, $qty, $tipe){
		$product = Product::where('id','=',$id_product)->first();
		if($tipe == 1){
			$stok = $product->stock;
		}else{
			$stok = $product->stock_eceran;
		}
		if($stok < $qty){
			return 0;
		}else{
			return 1;
		}
	}
    public static function increase_stock_purchase($model_detail,$param_column,$param_detail){
        $data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$quantity = $dd->qty;
			$product = Product::where('id','=',$id_product)->first();
    		$price = $dd->price / $product->kemasan;
			$product->stock = $product->stock + ($quantity * $product->kemasan);
			$product->price = $price;
			$product->update();
		}
    }
    public static function decrease_stock_purchase($model_detail,$param_column,$param_detail){
        $data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$quantity = $dd->qty;
			$product = Product::where('id','=',$id_product)->first();
			$price = $dd->price / $product->kemasan;
			$product->stock = $product->stock - ($quantity * $product->kemasan);
			$product->last_sell_price = $price;
			$product->update();
		}
    }
    public static function increase_stock_sale($model_detail,$param_column,$param_detail){
        $data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$quantity = $dd->qty;
			$product = Product::where('id','=',$id_product)->first();
    		$price = $dd->price / $product->kemasan;
			if($dd->tipe == "Utuh"){
				$product->stock = $product->stock + ($quantity * $product->kemasan);
			}else{
				$product->stock_eceran = $product->stock_eceran + $quantity;
			}
			$product->price = $price;
			$product->update();
		}
    }
    public static function decrease_stock_sale($model_detail,$param_column,$param_detail){
        $data_detail = $model_detail::where($param_column,'=',$param_detail)->get();
		foreach($data_detail as $dd){
			$id_product = $dd->product_id;
    		$quantity = $dd->qty;
			$product = Product::where('id','=',$id_product)->first();
			$price = $dd->price / $product->kemasan;
			if($dd->tipe == "Utuh"){
				$product->stock = $product->stock - ($quantity * $product->kemasan);
			}else{
				$product->stock_eceran = $product->stock_eceran - $quantity;
			}
			$product->last_sell_price = $price;
			$product->update();
		}
    }
	public static function check_balance($id_account, $total){
		$account = Account::where('id','=',$id_account)->first();
		$saldo = $account->balance;
		if($saldo < $total){
			return 0;
		}else{
			return 1;
		}
	}
	public static function increase_balance($model_payment,$id_account,$total, $model_head, $head_column, $param_head, $type){
		$total_payment = $model_payment::where($head_column,'=',$param_head)->get()->sum('total');
		$account = Account::where('id','=',$id_account)->first();
		$account->balance = $account->balance + $total;
		$account->update();
		$total_paid = $model_head::where('id',$param_head)->first();
		$total_paid->total_payment = $total_payment;
		if($total_paid->total_payment < $total_paid->grand_total){
			$total_paid->payment_st = "Belum lunas";
		}else{
			$total_paid->payment_st = "Lunas";
		}
		$total_paid->update();
		$jurnal = new Journal;
		$jurnals = new Journal;
		if($type == "Pembelian"){
			$akun = Account::where('id',12)->first();
			$akun->balance = $akun->balance ? $akun->balance:0 + $total_payment;
			$akun->update();
			$jurnal->type = 'P';
			$jurnal->account_id = 12;
			$jurnals->type = 'P';
			$jurnals->account_id = 1;
		}
		elseif($type == "Penjualan"){
			$akun = Account::where('id',13)->first();
			$akun->balance = $akun->balance ? $akun->balance:0 + $total_payment;
			$akun->update();
			$jurnal->type = 'S';
			$jurnal->account_id = 1;
			$jurnals->type = 'S';
			$jurnals->account_id = 13;
		}
		elseif($type == "Pengeluaran"){
			$akun = Account::where('id',$total_paid->expense_category_id)->first();
			$akun->balance = $akun->balance ? $akun->balance:0 + $total_payment;
			$akun->update();
			$jurnal->type = 'E';
			$jurnal->account_id = $total_paid->expense_category_id;
			
			$jurnals->type = 'E';
			$jurnals->account_id = 1;
		}
		$jurnal->transaction_id = $param_head;
		$jurnals->transaction_id = $param_head;
		$jurnal->date = date('Y-m-d');
		$jurnals->date = date('Y-m-d');
		$jurnal->debit = $total_payment;
		$jurnals->credit = $total_payment;
		$jurnal->save();
		$jurnals->save();
	}
	public static function decrease_balance($model_payment,$id_account,$total, $model_head, $head_column, $param_head, $type){
		$total_payment = $model_payment::where($head_column,'=',$param_head)->get()->sum('total');
		$account = Account::where('id','=',$id_account)->first();
		$account->balance = $account->balance - $total;
		$account->update();
		$total_paid = $model_head::where('id',$param_head)->first();
		$total_paid->total_payment = $total_payment;
		if($total_paid->total_payment < $total_paid->grand_total){
			$total_paid->payment_st = "Belum lunas";
		}else{
			$total_paid->payment_st = "Lunas";
		}
		$total_paid->update();
		if($type == "Pembelian"){
			$akun = Account::where('id',12)->first();
			$akun->balance = $akun->balance - $total;
			$akun->update();
			Journal::where('transaction_id',$param_head)->where('type','P')->delete();
		}
		elseif($type == "Penjualan"){
			$akun = Account::where('id',13)->first();
			$akun->balance = $akun->balance - $total;
			$akun->update();
			Journal::where('transaction_id',$param_head)->where('type','S')->delete();
		}
		elseif($type == "Pengeluaran"){
			$akun = Account::where('id',$total_paid->expense_category_id)->first();
			$akun->balance = $akun->balance - $total;
			$akun->update();
			Journal::where('transaction_id',$param_head)->where('type','E')->delete();
		}
	}
}