<!DOCTYPE html>
<html lang="en">
    @include('theme.auth.head')
	<body id="kt_body" class="bg-body">
		<div class="d-flex flex-column flex-root">
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url({{asset('keenthemes/media/illustrations/sigma-1/14.png')}}">
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					{{-- <a href="javascript:;" class="mb-12">
						<img alt="Logo" src="{{asset('keenthemes/media/logos/logo-1.svg')}}" class="h-40px" />
					</a> --}}
					{{$slot}}
				</div>
			</div>
		</div>
		@include('theme.auth.js')
        @yield('custom_js')
	</body>
</html>