<script>var hostUrl = "assets/";</script>
<!--begin::Global Javascript Bundle(used by all pages)-->
<script src="{{asset('keenthemes/plugins/global/plugins.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/scripts.bundle.js')}}"></script>
<!--end::Global Javascript Bundle-->
<!--begin::Page Vendors Javascript(used by this page)-->
<script src="{{asset('keenthemes/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>
<script src="{{asset('keenthemes/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<!--end::Page Vendors Javascript-->
<!--begin::Page Custom Javascript(used by this page)-->
<script src="{{asset('keenthemes/js/widgets.bundle.js')}}"></script>
<script src="{{asset('keenthemes/js/custom/widgets.js')}}"></script>
<script src="{{asset('js/method.js')}}"></script>
<script src="{{asset('js/plugin.js')}}"></script>
<!--end::Page Custom Javascript-->
<script>
    $('#mode_toggle').change(function () {
        if(this.value == "dark"){
            KTApp.setThemeMode("dark", function() {
                $("#dark_mode").hide();
                $("#light_mode").show();
            }); // set dark mode
            $("#mode_toggle").val('light');
        }else{
            KTApp.setThemeMode("light", function() {
                $("#light_mode").hide();
                $("#dark_mode").show();
            }); // set light mode
            $("#mode_toggle").val('dark');
        }
    });
    $("#light_mode").hide();
    function mode(type)
    {
        if(type == "dark"){
            $('#mode_toggle').prop('checked', true);
            KTApp.setThemeMode("dark", function() {
                $("#dark_mode").hide();
                $("#light_mode").show();
            }); // set dark mode
            $("#mode_toggle").val('light');
        }else{
            $('#mode_toggle').prop('checked', false);
            KTApp.setThemeMode("light", function() {
                $("#light_mode").hide();
                $("#dark_mode").show();
            }); // set light mode
            $("#mode_toggle").val('dark');
        }
    }
</script>