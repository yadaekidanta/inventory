@php
$persediaan_awal = $total_persediaan+$pa;
$persediaan_akhir = $total_persediaan;
$hpp = ($persediaan_awal + $pp->sum('total')) - $persediaan_akhir;
$gp = $sp->sum('total') - $hpp;
$tb = 0;
@endphp
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <tbody class="fw-bold text-gray-600">
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">AKTIVA</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">PASIVA</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Asset Lancar</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Kewajiban</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Kas</th>
            <th class="min-w-125px">Rp. {{number_format($kas->balance,2,",",".")}}</th>
            <th class="min-w-125px">Hutang Usaha</th>
            <th class="min-w-125px">Rp. {{number_format($hutang_dagang->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Persediaan</th>
            <th class="min-w-125px">Rp. {{number_format($persediaan_akhir,2,",",".")}}</th>
            <th class="min-w-125px">Hutang Gaji</th>
            <th class="min-w-125px">Rp. {{number_format($hutang_gaji->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Piutang</th>
            <th class="min-w-125px">Rp. {{number_format($piutang->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Total Asset Lancar</th>
            <th class="min-w-125px">Rp. {{number_format($kas->balance + $piutang->balance + $persediaan_akhir,2,",",".")}}</th>
            <th class="min-w-125px">Total Kewajiban</th>
            <th class="min-w-125px">Rp. {{number_format($hutang_dagang->balance + $hutang_gaji->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Asset Tetap</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Modal</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Peralatan</th>
            <th class="min-w-125px">Rp. {{number_format($peralatan->balance,2,",",".")}}</th>
            <th class="min-w-125px">Modal Saham</th>
            <th class="min-w-125px">Rp. {{number_format($modal->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Bangunan</th>
            <th class="min-w-125px">Rp. {{number_format($bangunan->balance,2,",",".")}}</th>
            <th class="min-w-125px">Laba di tahan</th>
            <th class="min-w-125px">Rp. {{number_format($laba->balance + $persediaan_akhir,2,",",".")}}</th>
        </tr>
        <tr class="text-start fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Total Asset Tetap</th>
            <th class="min-w-125px">Rp. {{number_format($peralatan->balance + $bangunan->balance,2,",",".")}}</th>
        </tr>
        <tr class="text-start fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Total Asset</th>
            <th class="min-w-125px">Rp. {{number_format($kas->balance + $piutang->balance + $peralatan->balance + $bangunan->balance + $persediaan_akhir,2,",",".")}}</th>
            <th class="min-w-125px">Total Modal</th>
            <th class="min-w-125px">Rp. {{number_format($modal->balance + $laba->balance + $persediaan_akhir,2,",",".")}}</th>
        </tr>
    </tbody>
</table>