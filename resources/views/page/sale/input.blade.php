<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Kode</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode..." value="{{$data->code}}" {{$data->code ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                        <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan tanggal..." value="{{$data->date}}">
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Kode Pelanggan</label>
                        <input type="hidden" class="form-control" id="pelanggan" name="pelanggan" placeholder="Masukkan nama..." value="{{$data->pelanggan_id}}">
                        <div class="input-group">
                            <input type="text" class="form-control" onkeyup="if (event.keyCode == 13) { getClient(this.value) }" id="kode_pelanggan" name="kode_pelanggan" placeholder="Masukkan kode pelanggan...">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <a href="javascript:;" onclick="handle_open_modal('{{route('app.client.get_list')}}','#modalPage','#contentListResult');">
                                        <i class="las la-search"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="fs-6 fw-bold mb-2">Nama pelanggan</label>
                        <input type="text" class="form-control" id="nama_pelanggan" readonly>
                    </div>
                    <div class="col-lg-8 mt-5">
                        <label class="fs-6 fw-bold mb-2">Alamat pelanggan</label>
                        <textarea class="form-control" id="alamat_pelanggan" readonly></textarea>
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.sale.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.sale.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if($data->id)
    <div class="row">
        <div class="col-3">
            <div class="card">
                <div class="card-body pb-5">
                    <h3>{{$data->code}}</h3>
                    <h5>{{$data->date->format('j F Y')}}</h5>
                    Nama Pelanggan : <h6>{{$data->client->name}}</h6>
                    Telp : <h6>{{$data->client->phone}}</h6>
                    Alamat : <h6>{{$data->client->address}}</h6>
                    Status : <h5>{{$data->st}}</h5>
                    <div class="min-w-150px mt-10 text-end">
                        <button onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</button>
                        @if ($data->st == 'Tertunda')
                        <button onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('app.sale.process',$data->id)}}');" class="btn btn-sm btn-success">Proses</button>
                        @elseif ($data->st == 'Dipesan')
                        <button onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','POST','{{route('app.sale.finish',$data->id)}}');" class="btn btn-sm btn-success">Diterima</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-9">
            @if ($data->st == "Tertunda")
            <div class="card">
                <div class="card-body pb-5">
                    <form id="form_input">
                        <div class="row">
                            <div class="col-lg-6">
                                <input type="hidden" class="form-control" name="id" value="{{$data->id}}">
                                <label class="required fs-6 fw-bold mb-2">Barang</label>
                                <input type="hidden" class="form-control" id="barang" name="barang" placeholder="Masukkan nama...">
                                <div class="input-group">
                                    <input type="text" class="form-control" onkeyup="if (event.keyCode == 13) { getBarang(this.value) }" id="kode_barang" name="kode_barang" placeholder="Masukkan kode barang...">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <a href="javascript:;" onclick="handle_open_modal('{{route('app.product.get_list')}}','#modalPage','#contentListResult');">
                                                <i class="las la-search"></i>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <label class="required fs-6 fw-bold mb-2">Nama Barang</label>
                                <input type="tel" class="form-control" id="nama_barang" name="nama" readonly>
                            </div>
                            <div class="col-lg-3 mt-5">
                                <label class="required fs-6 fw-bold mb-2">Harga Barang</label>
                                <input type="tel" class="form-control" id="harga_barang" name="harga">
                            </div>
                            <div class="col-lg-3 mt-5">
                                <label class="required fs-6 fw-bold mb-2">Satuan</label>
                                <select class="form-control" name="satuan">
                                    <option value="" selected disabled>Pilih Satuan</option>
                                    <option value="1">Utuh</option>
                                    <option value="2">Eceran</option>
                                </select>
                            </div>
                            <div class="col-lg-3 mt-5">
                                <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                                <input type="tel" class="form-control" name="qty">
                            </div>
                            <div class="col-lg-2 mt-5">
                                <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.sale-detail.store')}}','POST');" class="btn btn-sm btn-success mt-8">Tambah Barang</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5">
                            <thead>
                                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                    <th class="min-w-125px">Nama Barang</th>
                                    <th class="min-w-125px">Harga</th>
                                    <th class="min-w-125px">Jumlah</th>
                                    <th class="min-w-125px">Subtotal</th>
                                    <th class="text-end min-w-70px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="fw-bold text-gray-600">
                                @php
                                $total = 0;
                                $total_bayar = 0;
                                @endphp
                                @foreach ($data->detail as $item)
                                @php
                                $total += $item->subtotal;
                                @endphp
                                <tr>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->product->name}} {{$item->product->satuan->shortname}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{number_format($item->price)}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{number_format($item->qty)}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{number_format($item->subtotal)}}</a>
                                    </td>
                                    <td class="text-end">
                                        @if ($data->st == "Tertunda")
                                        <div class="btn-group" role="group">
                                            <button id="aksi" type="button" class="btn btn-sm btn-light btn-active-light-primary" data-bs-toggle="dropdown" aria-expanded="false">
                                                Aksi
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                                                    </svg>
                                                </span>
                                            </button>
                                            <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" aria-labelledby="aksi">
                                                <div class="menu-item px-3">
                                                    <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('app.sale-detail.destroy',$item->id)}}');" class="menu-link px-3">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="3">Total</td>
                                    <td>{{number_format($total)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @if ($data->st != "Tertunda")
            <div class="card">
                <div class="card-body pb-5">
                    <form id="form_input_payment">
                        <div class="row">
                            <div class="col-lg-5">
                                <input type="hidden" class="form-control" name="id" value="{{$data->id}}">
                                <label class="required fs-6 fw-bold mb-2">Akun</label>
                                <select class="form-control" name="akun">
                                    <option value="" SELECTED DISABLED>Pilih Akun</option>
                                    @foreach ( $akun as $item)
                                    <option value="{{$item->id}}">{{$item->code}} - {{$item->name}} | Rp {{number_format($item->balance)}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                                <input type="text" class="form-control" id="tanggal_bayar" name="tanggal_bayar">
                            </div>
                            <div class="col-lg-4">
                                <label class="required fs-6 fw-bold mb-2">Jumlah Bayar</label>
                                <input type="tel" class="form-control" id="jumlah_bayar" name="jumlah_bayar">
                            </div>
                            <div class="col-lg-4 mt-5">
                                <label class="fs-6 fw-bold mb-2">Catatan</label>
                                <textarea class="form-control" name="catatan"></textarea>
                            </div>
                            @if ($data->payment_st == "Belum lunas")
                            <div class="col-lg-4 mt-5">
                                <button id="tombol_simpan_payment" onclick="handle_save('#tombol_simpan_payment','#form_input_payment','{{route('app.sale-payment.store')}}','POST');" class="btn btn-sm btn-success mt-8">Tambah Pembayaran</button>
                            </div>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-middle table-row-dashed fs-6 gy-5">
                            <thead>
                                <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                    <th class="min-w-125px">Tanggal Bayar</th>
                                    <th class="min-w-125px">Akun</th>
                                    <th class="min-w-125px">Jumlah Bayar</th>
                                    <th class="min-w-125px">Catatan</th>
                                    <th class="text-end min-w-70px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody class="fw-bold text-gray-600">
                                @foreach ($data->payments as $item)
                                @php
                                $total_bayar += $item->total;
                                @endphp
                                <tr>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->date->format('j F Y')}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->account->name}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{number_format($item->total)}}</a>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->note}}</a>
                                    </td>
                                    <td class="text-end">
                                        @if ($data->st != "Tertunda")
                                        <div class="btn-group" role="group">
                                            <button id="aksi" type="button" class="btn btn-sm btn-light btn-active-light-primary" data-bs-toggle="dropdown" aria-expanded="false">
                                                Aksi
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                                                    </svg>
                                                </span>
                                            </button>
                                            <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" aria-labelledby="aksi">
                                                <div class="menu-item px-3">
                                                    <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('app.sale-payment.destroy',$item->id)}}');" class="menu-link px-3">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="2">Total</td>
                                    <td>{{number_format($total_bayar)}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
    @endif
</div>
<script>
    obj_enddatenow('tanggal');
    @if($data->id)
    getClient('{{$data->client->code}}')
    @endif
    function getClient(code){
        $.post("{{route('app.client.get')}}", {code:code}, function(result) {
            if (result.alert == "success") {
                $('#modalPage').modal('hide');
                $("#pelanggan").val(result.data.id);
                $("#kode_pelanggan").val(result.data.code);
                $("#nama_pelanggan").val(result.data.name);
                $("#alamat_pelanggan").val(result.data.address);
            }else{
                error_toastr(result.message);
            }
        }, "json");
    }
</script>