<x-app-layout title="Selamat datang">
    <div class="container-xxl" id="kt_content_container">
        <div class="card-p position-relative">
            <!--begin::Row-->
            <div class="row g-0">
                <!--begin::Col-->
                <div class="col px-4 py-8 rounded-2 me-7 mb-7 btn btn-hover-rise" style="background-color: #2EB3EC;">
                    <a href="{{route('app.account-type.index')}}" class="text-warning fw-bold fs-6">
                        <div class="row g-0">
                            <div class="col px-6 py-8 text-center">
                                <span class="svg-icon svg-icon-light svg-icon-4hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="currentColor"/>
                                <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="currentColor"/>
                                </svg></span>
                                <p class="fs-1 pt-2 text-white">Akun</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col px-4 py-8 rounded-2 me-7 mb-7 btn btn-hover-rise" style="background-color: #54B857;">
                    <a href="{{route('app.account.index')}}" class="text-warning fw-bold fs-6">
                        <div class="row g-0">
                            <div class="col px-6 py-8 text-center">
                                <span class="svg-icon svg-icon-light svg-icon-4hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M16.925 3.90078V8.00077L12.025 10.8008V5.10078L15.525 3.10078C16.125 2.80078 16.925 3.20078 16.925 3.90078ZM2.525 13.5008L6.025 15.5008L10.925 12.7008L6.025 9.90078L2.525 11.9008C1.825 12.3008 1.825 13.2008 2.525 13.5008ZM18.025 19.7008V15.6008L13.125 12.8008V18.5008L16.625 20.5008C17.225 20.8008 18.025 20.4008 18.025 19.7008Z" fill="currentColor"/>
                                <path opacity="0.3" d="M8.52499 3.10078L12.025 5.10078V10.8008L7.125 8.00077V3.90078C7.125 3.20078 7.92499 2.80078 8.52499 3.10078ZM7.42499 20.5008L10.925 18.5008V12.8008L6.02499 15.6008V19.7008C6.02499 20.4008 6.82499 20.8008 7.42499 20.5008ZM21.525 11.9008L18.025 9.90078L13.125 12.7008L18.025 15.5008L21.525 13.5008C22.225 13.2008 22.225 12.3008 21.525 11.9008Z" fill="currentColor"/>
                                </svg></span>
                                <p class="fs-1 pt-2 text-white">Jenis Akun</p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>