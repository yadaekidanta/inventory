<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Nama</label>
                        <input type="text" class="form-control" name="nama" placeholder="Masukkan nama..." value="{{$data->name}}">
                    </div>
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Kode / Nomor Referensi</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode / Nomor Referensi..." value="{{$data->code}}">
                    </div>
                    @if (!$data->id)
                    <div class="col-lg-6 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Saldo</label>
                        <input type="tel" class="form-control" id="saldo" name="saldo" placeholder="Masukkan Saldo..." value="{{number_format($data->balance)}}">
                    </div>
                    @endif
                    <div class="col-lg-6 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Jenis Akun</label>
                        <select class="form-control" name="tipe" id="tipe">
                            <option value="" selected disabled>Pilih Jenis Akun</option>
                            @foreach ($type as $item)
                                <option value="{{$item->id}}" {{$data->account_type_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.account.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.account.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    number_only('saldo');
    ribuan('saldo');
    obj_select('tipe', 'Pilih Jenis Akun');
</script>