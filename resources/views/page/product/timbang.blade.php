<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                        <input type="hidden" class="form-control" id="id" name="id" placeholder="Masukkan Jumlah..." value="{{$data->id}}">
                        <input type="text" class="form-control" id="jumlah" name="jumlah" placeholder="Masukkan Jumlah...">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.product.timbang.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    number_only('jumlah');
    ribuan('jumlah');
</script>