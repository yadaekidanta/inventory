<div class="modal-header header-bg">
    <h2 class="">List Barang</h2>
    <div class="btn btn-sm btn-icon btn-color-white btn-active-color-primary" data-bs-dismiss="modal">
        <span class="svg-icon svg-icon-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
            </svg>
        </span>
    </div>
</div>
<div class="modal-body scroll-y m-5">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
            <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                <th class="min-w-125px">SKU</th>
                <th class="min-w-125px">Nama</th>
                <th class="min-w-125px">Satuan</th>
                <th class="min-w-125px">Harga</th>
                <th class="min-w-125px">Harga Jual Terakhir</th>
                <th class="min-w-125px">Stok</th>
                <th class="text-end min-w-70px">Aksi</th>
            </tr>
        </thead>
        <tbody class="fw-bold text-gray-600">
            @if ($collection->count() > 0)
                @foreach ($collection as $item)
                <tr>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->sku}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->name}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->satuan->name}}</a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                            {{number_format($item->price ?: 0)}}
                        </a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                            {{number_format($item->last_sell_price)}}
                        </a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                            {{number_format($item->stock)}} @
                            @if($item->stock > 0)
                            {{$item->stock / $item->kemasan ?: 0}}
                            @endif
                        </a>
                    </td>
                    <td>
                        <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">
                            {{$item->stock_eceran}} @
                            @if($item->stock > 0)
                            {{$item->stock_eceran / $item->kemasan ?: 0}}
                            @endif
                        </a>
                    </td>
                    <td class="text-end">
                        <a href="javascript:;" onclick="getBarang('{{$item->sku}}');" class="menu-link px-3">Pilih</a>
                    </td>
                </tr>
                @endforeach
            @else
            <tr>
                <td colspan="9" class="text-center">
                    <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">Tidak ada data</a>
                </td>
            </tr>
            @endif
        </tbody>
    </table>
</div>