<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">SKU</label>
                        <input type="text" class="form-control" name="sku" placeholder="Masukkan SKU..." value="{{$data->sku}}" {{$data->sku ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Nama</label>
                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-2">
                        <label class="required fs-6 fw-bold mb-2">Satuan</label>
                        <select class="form-control" name="satuan" id="satuan">
                            <option value="" selected disabled>Pilih Satuan Barang</option>
                            @foreach ($satuan as $item)
                            <option value="{{$item->id}}" {{$data->product_unit_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <label class="required fs-6 fw-bold mb-2">Stok</label>
                        <input type="tel" class="form-control" id="stok" name="stok" placeholder="Masukkan Stok..." value="{{number_format($data->stock)}}">
                    </div>
                    <div class="col-lg-2">
                        <label class="required fs-6 fw-bold mb-2">Kemasan</label>
                        <input type="tel" class="form-control" id="kemasan" name="kemasan" placeholder="Masukkan Kemasan..." value="{{number_format($data->kemasan)}}">
                    </div>
                    <div class="col-lg-3">
                        <label class="required fs-6 fw-bold mb-2">Harga Beli</label>
                        <input type="tel" class="form-control" id="harga" name="harga" placeholder="Masukkan Harga Beli..." value="{{number_format($data->price)}}">
                    </div>
                    <div class="col-lg-3">
                        <label class="required fs-6 fw-bold mb-2">Harga Jual</label>
                        <input type="tel" class="form-control" id="harga_jual" name="harga_jual" placeholder="Masukkan Harga Jual..." value="{{number_format($data->last_sell_price)}}">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.product.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.product.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    obj_select('satuan', 'Pilih Satuan');
    number_only('stok');
    ribuan('stok');
    number_only('kemasan');
    ribuan('kemasan');
    number_only('harga');
    ribuan('harga');
    number_only('harga_jual');
    ribuan('harga_jual');
</script>