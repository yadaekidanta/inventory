<x-auth-layout title="Halaman masuk">
    <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
        <form class="form w-100" novalidate="novalidate" id="form_login">
            <div class="text-center mb-10">
                <h1 class="text-dark mb-3">Halaman masuk</h1>
                <div class="text-gray-400 fw-bold fs-4 d-none">New Here?
                <a href="../../demo17/dist/authentication/layouts/basic/sign-up.html" class="link-primary fw-bolder">Create an Account</a></div>
            </div>
            <div class="fv-row mb-10">
                <label class="form-label fs-6 fw-bolder text-dark">ID Pengguna</label>
                <input class="form-control form-control-lg form-control-solid" type="text" data-login="1" id="username" name="username" autocomplete="off" />
            </div>
            <div class="fv-row mb-10">
                <div class="d-flex flex-stack mb-2">
                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Kata Sandi</label>
                    <a href="javascript:;" class="link-primary fs-6 fw-bolder d-none">Lupa kata sandi ?</a>
                </div>
                <input class="form-control form-control-lg form-control-solid" type="password" data-login="2" name="password" autocomplete="off" />
            </div>
            <div class="text-center">
                <button id="tombol_login" onclick="handle_post('#tombol_login','#form_login','{{route('app.auth.login')}}');" class="btn btn-lg btn-primary w-100 mb-5">
                    <span class="indicator-label">Lanjutkan</span>
                    <span class="indicator-progress">Harap tunggu...
                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                </button>
            </div>
        </form>
    </div>
    @section('custom_js')
        <script type="text/javascript">
        $("#username").focus();
        </script>
    @endsection
</x-auth-layout>