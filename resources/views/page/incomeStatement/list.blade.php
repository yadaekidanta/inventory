@php
$persediaan_awal = $total_persediaan+$pa;
$persediaan_akhir = $total_persediaan;
$hpp = ($persediaan_awal + $pp->sum('total')) - $persediaan_akhir;
$gp = $sp->sum('total') - $hpp;
$tb = 0;
@endphp
<table class="table align-middle table-row-dashed fs-6 gy-5">
    <tbody class="fw-bold text-gray-600">
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Penjualan</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Rp. {{number_format($sp->sum('total'),2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Persediaan Awal</th>
            <th class="min-w-125px">Rp. {{number_format($persediaan_awal ,2,",",".")}}</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Pembelian</th>
            <th class="min-w-125px">Rp. {{number_format($pp->sum('total'),2,",",".")}}</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Persediaan Akhir</th>
            <th class="min-w-125px">Rp. {{number_format($persediaan_akhir ,2,",",".")}}</th>
            <th class="min-w-125px"></th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">HPP</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Rp. {{number_format($hpp,2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Gross Profit</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Rp. {{number_format($gp,2,",",".")}}</th>
        </tr>
        @foreach($ep as $e)
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Beban {{$e->account->name}} {{$e->expense->category->name}}</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Rp. {{number_format($e->expense->total_payment,2,",",".")}}</th>
        </tr>
        @php
        $tb += $e->expense->total_payment;
        @endphp
        @endforeach
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">Laba Bersih</th>
            <th class="min-w-125px"></th>
            <th class="min-w-125px">Rp. {{number_format($gp - $tb,2,",",".")}}</th>
        </tr>
    </tbody>
</table>