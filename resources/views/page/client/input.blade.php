<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Kode</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode..." value="{{$data->code}}" {{$data->code ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Nama</label>
                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Propinsi</label>
                        <select class="form-control" name="propinsi" id="propinsi">
                            <option value="" selected disabled>Pilih Propinsi</option>
                            @foreach ($propinsi as $item)
                                <option value="{{$item->id}}" {{$data->province_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Kota</label>
                        <select class="form-control" name="kota" id="kota">
                            <option value="" selected disabled>Pilih Propinsi terlebih dahulu</option>
                        </select>
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Kecamatan</label>
                        <select class="form-control" name="kecamatan" id="kecamatan">
                            <option value="" selected disabled>Pilih Kota terlebih dahulu</option>
                        </select>
                    </div>
                    <div class="col-lg-12 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Alamat</label>
                        <textarea class="form-control" name="alamat" id="alamat">{{$data->address}}</textarea>
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">No Telp 1</label>
                        <input type="tel" maxlength="13" class="form-control" id="telp_1" name="telp_1" value="{{$data->telp_1}}">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">No Telp 2</label>
                        <input type="tel" maxlength="13" class="form-control" id="telp_2" name="telp_2" value="{{$data->telp_2}}">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">No HP</label>
                        <input type="tel" maxlength="13" class="form-control" id="phone" name="phone" value="{{$data->phone}}">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.client.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.client.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    obj_select('propinsi', 'Pilih Propinsi');
    obj_select('kota', 'Pilih Kota');
    obj_select('kecamatan', 'Pilih Kecamatan');
    obj_autosize('alamat');
    number_only('telp_1');
    number_only('telp_2');
    number_only('phone');
    @if($data->province_id)
    $('#propinsi').val('{{$data->province_id}}');
    setTimeout(function(){ 
        $('#propinsi').trigger('change');
        setTimeout(function(){ 
            $('#kota').val('{{$data->city_id}}');
            $('#kota').trigger('change');
            setTimeout(function(){ 
                $('#kecamatan').val('{{$data->subdistrict_id}}');
                $('#kecamatan').trigger('change');
            }, 1500);
        }, 1200);
    }, 1000);
    @endif
    $("#propinsi").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('app.regional.get_city')}}",
            data: {propinsi : $("#propinsi").val()},
            success: function(response){
                $("#kota").html(response);
            }
        });
    });
    $("#kota").change(function(){
        $.ajax({
            type: "POST",
            url: "{{route('app.regional.get_subdistrict')}}",
            data: {kota : $("#kota").val()},
            success: function(response){
                $("#kecamatan").html(response);
            }
        });
    });
</script>