<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Nama</label>
                        <input type="text" class="form-control" name="name" placeholder="Masukkan nama..." value="{{$data->name}}">
                    </div>
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Email</label>
                        <input type="email" class="form-control" name="email" placeholder="Masukkan email..." value="{{$data->email}}">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">ID Pengguna</label>
                        <input type="text" class="form-control" name="id_pengguna" placeholder="Masukkan ID pengguna..." value="{{$data->username}}">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Kata Sandi</label>
                        <input type="password" class="form-control" name="kata_sandi" placeholder="Masukkan kata sandi...">
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="required fs-6 fw-bold mb-2">Role</label>
                        <select class="form-control" name="role" id="role">
                            <option value="" selected disabled>Pilih Role</option>
                            @foreach ($role as $item)
                                <option value="{{$item->id}}" {{$data->role_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.user.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.user.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    obj_select('role', 'Pilih Role');
</script>