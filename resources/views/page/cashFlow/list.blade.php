<table class="table align-middle table-row-dashed fs-6 gy-5">
    <tbody class="fw-bold text-gray-600">
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">+</th>
            <th class="min-w-125px">Pendapatan</th>
            <th class="min-w-125px">Rp. {{number_format($sp->sum('total'),2,",",".")}}</th>
        </tr>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">-</th>
            <th class="min-w-125px">Kas Pembelian Barang</th>
            <th class="min-w-125px">Rp. {{number_format($pp->sum('total'),2,",",".")}}</th>
        </tr>
        @foreach($ep as $e)
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="min-w-125px">-</th>
            <th class="min-w-125px">{{$e->account->name}} {{$e->expense->category->name}}</th>
            <th class="min-w-125px">Rp. {{number_format($e->expense->total_payment,2,",",".")}}</th>
        </tr>
        @endforeach
    </tbody>
</table>