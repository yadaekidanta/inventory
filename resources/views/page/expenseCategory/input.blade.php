<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Kode</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode..." value="{{$data->code}}" {{$data->code ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Nama</label>
                        <input type="text" class="form-control" name="nama" placeholder="Masukkan nama..." value="{{$data->name}}">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.expense-category.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.expense-category.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>