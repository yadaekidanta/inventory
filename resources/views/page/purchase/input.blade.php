<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Kode</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode..." value="{{$data->code}}" {{$data->code ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                        <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan tanggal..." value="{{$data->date}}">
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Kode Supplier</label>
                        <input type="hidden" class="form-control" id="supplier" name="supplier" placeholder="Masukkan nama..." value="{{$data->supplier_id}}">
                        <div class="input-group">
                            <input type="text" class="form-control" onkeyup="if (event.keyCode == 13) { getSupplier(this.value) }" id="kode_supplier" name="kode_supplier" placeholder="Masukkan kode supplier...">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <a href="javascript:;" onclick="handle_open_modal('{{route('app.supplier.get_list')}}','#modalPage','#contentListResult');">
                                        <i class="las la-search"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 mt-5">
                        <label class="fs-6 fw-bold mb-2">Nama Supplier</label>
                        <input type="text" class="form-control" id="nama_supplier" readonly>
                    </div>
                    <div class="col-lg-8 mt-5">
                        <label class="fs-6 fw-bold mb-2">Alamat Supplier</label>
                        <textarea class="form-control" id="alamat_supplier" readonly></textarea>
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.purchase.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_save('#tombol_simpan','#form_input','{{route('app.purchase.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    obj_enddatenow('tanggal');
    @if($data->id)
    getSupplier('{{$data->supplier->code}}')
    @endif
    function getSupplier(code){
        $.post("{{route('app.supplier.get')}}", {code:code}, function(result) {
            if (result.alert == "success") {
                $('#modalPage').modal('hide');
                $("#supplier").val(result.data.id);
                $("#kode_supplier").val(result.data.code);
                $("#nama_supplier").val(result.data.name);
                $("#alamat_supplier").val(result.data.address);
            }else{
                error_toastr(result.message);
            }
        }, "json");
    }
</script>