<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-4">
                        <label class="fs-6 fw-bold mb-2">Jenis</label>
                        <select class="form-control" name="jenis_pengeluaran" id="jenis_pengeluaran">
                            <option value="" selected disabled>Pilih Jenis</option>
                            <option value="D" {{$data->expense_category_id == "D" ? 'selected' : ''}}>Debit</option>
                            <option value="K" {{$data->expense_category_id == "K" ? 'selected' : ''}}>Kredit</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                        <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan tanggal..." value="{{$data->date}}">
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Jumlah</label>
                        <input type="text" class="form-control" id="total_pengeluaran" name="total_pengeluaran" placeholder="Masukkan Total Pengeluaran..." value="{{number_format($data->grand_total)}}">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('app.expense.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('app.expense.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    obj_enddatenow('tanggal');
    number_only('total_pengeluaran');
    ribuan('total_pengeluaran');
    obj_select('jenis_pengeluaran', 'Pilih Jenis Pengeluaran');
    obj_enddatenow('tanggal_bayar');
    number_only('jumlah_bayar');
    ribuan('jumlah_bayar');
</script>