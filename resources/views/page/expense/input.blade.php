<div id="kt_content_container" class="container-xxl">
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input">
                <div class="row">
                    <div class="col-lg-6">
                        <label class="required fs-6 fw-bold mb-2">Kode</label>
                        <input type="text" class="form-control" name="kode" placeholder="Masukkan Kode..." value="{{$data->code}}" {{$data->code ? 'readonly': ''}}>
                    </div>
                    <div class="col-lg-6">
                        <label class="fs-6 fw-bold mb-2">Jenis Pengeluaran</label>
                        <select class="form-control" name="jenis_pengeluaran" id="jenis_pengeluaran">
                            <option value="" selected disabled>Pilih Jenis Pengeluaran</option>
                            @foreach ($category as $item)
                            <option value="{{$item->id}}" {{$data->expense_category_id == $item->id ? 'selected' : ''}}>{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                        <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Masukkan tanggal..." value="{{$data->date}}">
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Total Pengeluaran</label>
                        <input type="text" class="form-control" id="total_pengeluaran" name="total_pengeluaran" placeholder="Masukkan Total Pengeluaran..." value="{{number_format($data->grand_total)}}">
                    </div>
                    <div class="col-lg-4">
                        <label class="fs-6 fw-bold mb-2">Dokumen Pendukung</label>
                        <input type="file" class="form-control" accept="application/pdf" id="dokumen" name="dokumen" placeholder="Masukkan Dokumen Pendukung...">
                    </div>
                    <div class="min-w-150px mt-10 text-end">
                        <a href="javascript:;" onclick="load_list(1);" class="btn btn-sm btn-primary">Kembali</a>
                        @if ($data->id)
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('app.expense.update',$data->id)}}','PATCH');" class="btn btn-sm btn-warning text-black">Simpan</button>
                        @else
                        <button id="tombol_simpan" onclick="handle_upload('#tombol_simpan','#form_input','{{route('app.expense.store')}}','POST');" class="btn btn-sm btn-success">Simpan</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
    @if($data->id)
    <div class="card">
        <div class="card-body pb-5">
            <form id="form_input_payment">
                <div class="row">
                    <div class="col-lg-5">
                        <input type="hidden" class="form-control" name="id" value="{{$data->id}}">
                        <label class="required fs-6 fw-bold mb-2">Akun</label>
                        <select class="form-control" name="akun">
                            <option value="" SELECTED DISABLED>Pilih Akun</option>
                            @foreach ( $akun as $item)
                            <option value="{{$item->id}}">{{$item->code}} - {{$item->name}} | Rp {{number_format($item->balance)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label class="required fs-6 fw-bold mb-2">Tanggal</label>
                        <input type="text" class="form-control" id="tanggal_bayar" name="tanggal_bayar">
                    </div>
                    <div class="col-lg-4">
                        <label class="required fs-6 fw-bold mb-2">Jumlah Bayar</label>
                        <input type="tel" class="form-control" id="jumlah_bayar" name="jumlah_bayar">
                    </div>
                    @if ($data->payment_st == "Belum lunas")
                    <div class="col-lg-4 mt-5">
                        <button id="tombol_simpan_payment" onclick="handle_save('#tombol_simpan_payment','#form_input_payment','{{route('app.expense-payment.store')}}','POST');" class="btn btn-sm btn-success mt-8">Tambah Pembayaran</button>
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table align-middle table-row-dashed fs-6 gy-5">
                    <thead>
                        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                            <th class="min-w-125px">Tanggal Bayar</th>
                            <th class="min-w-125px">Akun</th>
                            <th class="min-w-125px">Jumlah Bayar</th>
                            <th class="text-end min-w-70px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody class="fw-bold text-gray-600">
                        @php
                        $total_bayar = 0;
                        @endphp
                        @foreach ($data->payments as $item)
                        @php
                        $total_bayar += $item->total;
                        @endphp
                        <tr>
                            <td>
                                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->date->format('j F Y')}}</a>
                            </td>
                            <td>
                                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{$item->account->name}}</a>
                            </td>
                            <td>
                                <a href="javascript:;" class="text-gray-800 text-hover-primary mb-1">{{number_format($item->total)}}</a>
                            </td>
                            <td class="text-end">
                                @if ($data->st != "Tertunda")
                                <div class="btn-group" role="group">
                                    <button id="aksi" type="button" class="btn btn-sm btn-light btn-active-light-primary" data-bs-toggle="dropdown" aria-expanded="false">
                                        Aksi
                                        <span class="svg-icon svg-icon-5 m-0">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                                            </svg>
                                        </span>
                                    </button>
                                    <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" aria-labelledby="aksi">
                                        <div class="menu-item px-3">
                                            <a href="javascript:;" onclick="handle_confirm('Apakah Anda Yakin?','Yakin','Tidak','DELETE','{{route('app.expense-payment.destroy',$item->id)}}');" class="menu-link px-3">Hapus</a>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="2">Total</td>
                            <td>{{number_format($total_bayar)}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
</div>
<script>
    obj_enddatenow('tanggal');
    number_only('total_pengeluaran');
    ribuan('total_pengeluaran');
    obj_select('jenis_pengeluaran', 'Pilih Jenis Pengeluaran');
    obj_enddatenow('tanggal_bayar');
    number_only('jumlah_bayar');
    ribuan('jumlah_bayar');
</script>